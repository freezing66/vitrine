<div class="col">
    <div class="card shadow-sm">
        <svg 
            class="bd-placeholder-img card-img-top" 
            width="100%" 
            height="125" 
            xmlns="http://www.w3.org/2000/svg" 
            role="img" 
            aria-label="Placeholder: Thumbnail" 
            preserveAspectRatio="xMidYMid slice" f
            ocusable="false">
            <title>Placeholder</title>
            <rect width="100%" height="100%" fill="#55595c"></rect>
            <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
        </svg>
        <div class="card-body">
        <p>{{ product.name }}</p>
        <p>Prix à l'<strong>unité</strong> :{{ product.pricePerUnit }}</p>
        <p>Prix au <strong>kilo</strong> :{{ product.pricePerKg }}</p>
        {% if product.unit %}
            <small></small>
        {% elseif not product.unit %}
            <small></small>
        {% else %}
            <small></small>
        {% endif %}
        <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
            <a href={{ path('app_order',{'name' : product.name}) }} product.name class="btn btn-sm btn-outline-secondary" tabindex="-1" role="button" aria-disabled="true">👁</a>
            {% if product.unit %}
                <a href={{ path('cart_add',{'id' : product.id,'route' : 'home'}) }} product.name class="btn btn-sm btn-outline-secondary" tabindex="-1" role="button" aria-disabled="true">+🛒</a>
            {% else %}
                <a href={{ path('cart_add',{'id' : product.id,'route' : 'cart'}) }} product.name class="btn btn-sm btn-outline-secondary" tabindex="-1" role="button" aria-disabled="true">+🛒</a>
            {% endif %}
        </div>
        </div>
        </div>
    </div>
</div>