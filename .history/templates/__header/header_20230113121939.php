<div class="container-fluid d-grid gap-3 align-items-center" style="grid-template-columns: 1fr 2fr;">
    <img src="data/images/Intermarché_Logo.png" class="rounded float-start" alt="intermarché">


    <div class="d-flex align-items-center">
        <form class="w-100 me-3" role="search">
            <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>
        <button class="btn" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
        ☰
        </button>
      	<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        	<div class="offcanvas-header">
          	<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        	</div>
        	<div class="offcanvas-body">
				<div>
					<h2>Menu</h2>
				</div>
          
        	</div>
      	</div>
    </div>
</div>
  