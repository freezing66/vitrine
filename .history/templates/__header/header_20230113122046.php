<div class="container-fluid d-grid gap-3 align-items-center" style="grid-template-columns: 1fr 2fr;">
    <img src="data/images/Intermarché_Logo.png" class="rounded float-start" alt="intermarché">


    <div class="d-flex align-items-center">
        <form class="w-100 me-3" role="search">
            <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>
        <button class="btn" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
        ☰
        </button>
      	<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        	<div class="offcanvas-header">
          	<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        	</div>
        	<div class="offcanvas-body">
				<div>
					<h2>Menu</h2>
				</div>
				<div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 280px;">
					<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
					<svg class="bi pe-none me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
					<span class="fs-4">Sidebar</span>
					</a>
					<hr>
					<ul class="nav nav-pills flex-column mb-auto">
					<li class="nav-item">
						<a href="#" class="nav-link active" aria-current="page">
						<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#home"></use></svg>
						Home
						</a>
					</li>
					<li>
						<a href="#" class="nav-link link-dark">
						<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
						Dashboard
						</a>
					</li>
					<li>
						<a href="#" class="nav-link link-dark">
						<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#table"></use></svg>
						Orders
						</a>
					</li>
					<li>
						<a href="#" class="nav-link link-dark">
						<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#grid"></use></svg>
						Products
						</a>
					</li>
					<li>
						<a href="#" class="nav-link link-dark">
						<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg>
						Customers
						</a>
					</li>
					</ul>
					<hr>
					<div class="dropdown">
					<a href="#" class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
						<img src="https://github.com/mdo.png" alt="" class="rounded-circle me-2" width="32" height="32">
						<strong>mdo</strong>
					</a>
					<ul class="dropdown-menu text-small shadow">
						<li><a class="dropdown-item" href="#">New project...</a></li>
						<li><a class="dropdown-item" href="#">Settings</a></li>
						<li><a class="dropdown-item" href="#">Profile</a></li>
						<li><hr class="dropdown-divider"></li>
						<li><a class="dropdown-item" href="#">Sign out</a></li>
					</ul>
					</div>
				</div>
        	</div>
      	</div>
    </div>
</div>
  