<div class="container-fluid d-grid gap-3 align-items-center" style="grid-template-columns: 1fr 2fr;">
    <img src="data/images/Intermarché_Logo.png" class="rounded float-start" alt="intermarché">


    <div class="d-flex align-items-center">
        <form class="w-100 me-3" role="search">
            <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>

        <button class="btn btn-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
          Button with data-bs-target
        </button>
      
    </div>
</div>
  