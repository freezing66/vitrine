
<div class="d-flex flex-column flex-shrink-0 bg-light" style="width: 4.5rem;">
    <button class="btn border-bottom" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
        ☰
    </button>

    
    <ul class="nav nav-pills nav-flush flex-column mb-auto text-center">
        <li class="nav-item">
            <a href="{{ path('app_home') }}" class="nav-link py-3 border-bottom rounded-0 link-dark fs-4" aria-current="page" data-bs-toggle="tooltip" data-bs-placement="right" aria-label="Home" data-bs-original-title="Home"></a>
        </li>
        <li>
            <a href="{{ path('cart') }}" class="nav-link py-3 border-bottom rounded-0" data-bs-toggle="tooltip" data-bs-placement="right" aria-label="Dashboard" data-bs-original-title="Dashboard">
                🛒
            </a>
        </li>
        <li>
            <a href="#" class="nav-link py-3 border-bottom rounded-0" data-bs-toggle="tooltip" data-bs-placement="right" aria-label="Orders" data-bs-original-title="Orders">
            </a>
        </li>
        <li>
        <a href="#" class="nav-link py-3 border-bottom rounded-0" data-bs-toggle="tooltip" data-bs-placement="right" aria-label="Products" data-bs-original-title="Products">
        </a>
        </li>
        <li>
            <a href="#" class="nav-link py-3 border-bottom rounded-0" data-bs-toggle="tooltip" data-bs-placement="right" aria-label="Customers" data-bs-original-title="Customers">
            </a>
        </li>
    </ul>
</div>
<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header">
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
		<div>
			<h2>Menu</h2>
		</div>
		<div class="d-flex flex-column flex-shrink-0 p-3" style="width: 280px;">
			<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
			</a>
			<ul class="nav nav-pills flex-column mb-auto">
						
				<hr>
					<li>
					<a href="{{ path('cart') }}" class="nav-link link-dark">
					<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg>
                        🛒 panier
					</a>
				</li>
			</ul>
		</div>
    </div>
</div>