<button class="btn" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
    ☰
</button>
<a href="{{ path('cart') }}" class="nav-link link-dark">
    <svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg>
	🛒
</a>
<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    <div class="offcanvas-header">
    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
		<div>
			<h2>Menu</h2>
		</div>
		<div class="d-flex flex-column flex-shrink-0 p-3" style="width: 280px;">
			<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
			</a>
			<ul class="nav nav-pills flex-column mb-auto">
						
				<hr>
					<li>
					<a href="{{ path('cart') }}" class="nav-link link-dark">
					<svg class="bi pe-none me-2" width="16" height="16"><use xlink:href="#people-circle"></use></svg>
						Mon Panier
					</a>
				</li>
			</ul>
		</div>
    </div>
</div>