<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart', name: 'cart_add')]
    public function add(SessionInterface $session): Response
    {
        $cart =$session->get('cart', []);

        if(!empty($cart[$_GET['product']]))
        {
            $cart[$_GET['product']] += 1;
        } else {
            $cart[$_GET['product']] = 1;
        }
        dd($cart)
        if (isset($_GET['product']) && isset($_GET['quantity'])){
            return $this->render('cart/index.html.twig', [
                'controller_name' => 'CartController',
                'info' => [$_GET['product'],$_GET['quantity']]
            ]);
        }
    }
}
