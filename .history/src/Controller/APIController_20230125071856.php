<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\OrderRepository;
use App\Entity\Order;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends AbstractController
{
    #[Route('/api_get_cart', name: 'app_api_get_cart')]
    public function getCart(SessionInterface $session,OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPricePerKg() * $quantity;
        }
        return new Response(json_encode($data));
    }

    #[Route('/api_add_cart?id={id}', name: 'app_api_add_cart')]
    public function add(Prduct $order, SessionInterface $session): Response
    {
        $cart=$session->get('cart', []);

        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        
        $session->set('cart',$cart);
        return new Response(json_encode(true));
    }
}
