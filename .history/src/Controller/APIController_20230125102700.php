<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class APIController extends AbstractController
{
    #[Route('/api_get_cart', name: 'app_api_get_cart')]
    public function getCart(SessionInterface $session,ProductRepository $productRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $productRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPrice() * $quantity;
        }
        return new Response(json_encode($data));
    }

    #[Route('/api_add_cart?id={id}', name: 'app_api_add_cart')]
    public function add(Product $product, SessionInterface $session): Response
    {
        $cart=$session->get('cart', []);

        if (empty($cart[$product->getId()]))
        {
            $cart[$product->getId()] = 0;
        }
        $cart[$product->getId()] += 1;
        
        $session->set('cart',$cart);
        return new Response(json_encode(true));
    }

    #[Route('/api_delete_cart?id={id}', name: 'app_api_delete_cart')]
    public function delete($id,
        SessionInterface $session, 
        ProductRepository $productRepo
    ): Response {
        $cart =$session->get('cart', []);

        if($product = $productRepo->find($id))
        {
            if($cart[$product->getId()] > 1)
            {
                $cart[$product->getId()] -= 1;
            } else {
                unset($cart[$product->getId()]);
            }
        }
        $session->set('cart',$cart);
        return new Response(json_encode(true));
    }
    #[Route('/api_unset_cart?id={id}', name: 'app_api_unset_cart')]
    public function unset($id, 
        SessionInterface $session, 
        ProductRepository $productRepo
    ): Response {
        $cart =$session->get('cart', []);
        if ($cart[$id]){
            unset($cart[$id]);
        }
        $session->set('cart',$cart);
        return new Response(json_encode(true));
    }

    #[Route('/api_save_cookie', name: 'app_api_save_cookie')]
    public function save_ref(SessionInterface $session): Response
    {
        $cookie = $session->get['cookie'];

        $res = new Response();
        $res->headers->setCookie( $cookie );
        $res->send();
        return $res;
    }
}
