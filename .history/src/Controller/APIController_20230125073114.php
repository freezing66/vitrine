<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class APIController extends AbstractController
{
    #[Route('/api_get_cart', name: 'app_api_get_cart')]
    public function getCart(SessionInterface $session,ProductRepository $productRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $productRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPrice() * $quantity;
        }
        return new Response(json_encode($data));
    }

    #[Route('/api_add_cart?id={id}', name: 'app_api_add_cart')]
    public function add(Product $product, SessionInterface $session): Response
    {
        $cart=$session->get('cart', []);

        if (empty($cart[$product->getId()]))
        {
            $cart[$product->getId()] = 0;
        }
        $cart[$product->getId()] += 1;
        
        $session->set('cart',$cart);
        return new Response(json_encode(true));
    }
    
    #[Route('/cart_delete?id={id}&all={all}', name: 'cart_delete')]
    public function delete($id, $all, 
        SessionInterface $session, 
        ProductRepository $productRepo
    ): Response {
        $cart =$session->get('cart', []);
        if ($all == true)
        {
            unset($cart[$id]);
        } else {
            if($product = $productRepo->find($id))
            {
                if($cart[$product->getId()] > 1)
                {
                    $cart[$product->getId()] -= 1;
                } else {
                    unset($cart[$product->getId()]);
                }
            }
            
        }
        $session->set('cart',$cart);
        return $this->redirectToRoute('app_home');
    }
}
