<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Entity\Order;
use App\Entity\Facture;
use App\Form\MailerForm;
use App\Service\Mailer;

use Symfony\Component\Uid\Factory\UlidFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart_add?id={id}&route={route}', name: 'cart_add')]
    public function add(Order $order,$route, SessionInterface $session): Response
    {
        
        $cart =$session->get('cart', []);

        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
        
    }
    #[Route('/cart_delete?id={id}&route={route}&all={all}', name: 'cart_delete')]
    public function delete($id, $route, $all, SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart =$session->get('cart', []);
        if ($all == true)
        {
            unset($cart[$id]);
        } else {
            if($order = $orderRepo->find($id))
            {
                if($cart[$order->getId()] > 1)
                {
                    $cart[$order->getId()] -= 1;
                } else {
                    unset($cart[$order->getId()]);
                }
            }
            
        }
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
    }
    #[Route('/cart', name: 'cart')]
    public function show(SessionInterface $session, OrderRepository $orderRepo, Mailer $email, Request $request): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg() * $quantity;
        }
        $facture = new Facture();
        $facture->generate();
        $facture->setCart($data);
        $form = $this->createFormBuilder($facture)
            ->add('reference', HiddenType::class)
            ->add('email', EmailType::class)
            ->add('date', HiddenType::class)
            ->add('save', SubmitType::class)->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $dataForm = $form->getData();
            $dataForm->setDate(new \DateTime('tomorrow'));
            $email->send(
                'symfonymatete@gmail.com',
                $dataForm->getEmail(),
                'Commande passé avec succès',
                'index',
                [
                    $dataForm->getCart()
                    //$dataForm->getReference(),
                    //strlen($dataForm->getDate())
                ]
                    
            );
            dd($email);

            // ... perform some action, such as saving the task to the database
            $this->addFalsh('sucess','La commande a été passé avec succès, un mailvous a été envoyé à l\'adresse suivante :'+$data->getEmail());
            
        }

        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            'form' => $form,
            'total' => $total,
            'data' => $data
            
        ]);
    }
}
