<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Entity\Order;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart_add?id={id}', name: 'cart_add')]
    public function add(Order $order = null, SessionInterface $session): Response
    {
        
        $cart =$session->get('cart', []);

        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        
        $session->set('cart',$cart);
        return $this->redirectToRoute('app_home');
    }
    #[Route('/cart', name: 'cart')]
    public function show(SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg();
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            'cart' => $data
        ]);
    }
}
