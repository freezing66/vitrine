<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cart')]
class CartController extends AbstractController
{
    #[Route('/cart', name: 'app_cart')]
    public function index(): Response
    {
        if (isset($_GET['product']) && isset($_GET['quantity'])){
            return $this->render('cart/index.html.twig', [
                'controller_name' => 'CartController',
                'info' => [$_GET['product'],$_GET['quantity']]
            ]);
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'info' => ['salut','quantity']
        ]);
    }
}
