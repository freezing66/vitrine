<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route('/product?name={name}', name: 'app_order')]
    public function index($name): Response
    {

        return $this->render('order/index.html.twig', [
            'controller_name' => 'ProductController',
            'name' => $name
        ]);
    }
}
