<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Entity\Order;
use App\Entity\Facture;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart_add?id={id}&route={route}', name: 'cart_add')]
    public function add(Order $order,$route, SessionInterface $session): Response
    {
        
        $cart =$session->get('cart', []);

        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
        
    }
    #[Route('/cart_delete?id={id}&route={route}&all={all}', name: 'cart_delete')]
    public function delete($id, $route, $all, SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart =$session->get('cart', []);
        if ($all == true)
        {
            unset($cart[$id]);
        } else {
            if($order = $orderRepo->find($id))
            {
                if($cart[$order->getId()] > 1)
                {
                    $cart[$order->getId()] -= 1;
                } else {
                    unset($cart[$order->getId()]);
                }
            }
            
        }
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
    }
    #[Route('/cart', name: 'cart')]
    public function show(SessionInterface $session, OrderRepository $orderRepo, Mailer $mailer): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg() * $quantity;
        }
        $facture = new Facture();
        $form = $this->createForm(Mailer::class, $facture);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $data = $form->getData();

            // ... perform some action, such as saving the task to the database
            $this->addFalsh('sucess','Commande passé avec succès, un mailvous a été envoyé à l\'adresse suivante :'+$data->getEmail());
            $email->send(
                'symfonymatete@gmail.com',
                $data->getEmail(),
                'Commande passé avec succès',
                'index',
                compact($data)
            );
            $this->mailer->send();
        }

        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            'data' => $data,
            'total' => $total,
            'form' => $form
        ]);
    }
}
