<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Entity\Order;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart_add?id={id}&route={route}', name: 'cart_add')]
    public function add(Order $order,$route, SessionInterface $session): Response
    {
        
        $cart =$session->get('cart', []);

        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
        
    }
    #[Route('/cart_delete?id={id}', name: 'cart_delete')]
    public function delete($id, SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart =$session->get('cart', []);
        if($id == "all")
        {
            unset($cart[$id]);
        } else {
            if($order = $orderRepo->find($id))
            {
                if($cart[$order->getId()] && $cart[$order->getId()] >= 1)
                {
                    $cart[$order->getId()] -= 1;
                } else {
                    unset($cart[$order->getId()]);
                }
            }
            
        }
        
        $session->set('cart',$cart);
        switch ($route) {
            case 'home':
                return $this->redirectToRoute('app_home');
                break;
            case 'cart':
                return $this->redirectToRoute('cart');
                break;
            case 2:
                return $this->redirectToRoute('app_home');
                break;
        }
    }
    #[Route('/cart', name: 'cart')]
    public function show(SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg() * $quantity;
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            'data' => $data,
            'total' => $total
        ]);
    }
}
