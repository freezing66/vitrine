<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Entity\Product;
use App\Entity\Facture;
use App\Form\MailerForm;
use App\Service\Mailer;

use Symfony\Component\Uid\Factory\UlidFactory;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{   
    #[Route('/cart_delete?id={id}&all={all}', name: 'cart_delete')]
    public function delete($id, $all, SessionInterface $session, ProductRepository $orderRepo): Response
    {
        $cart =$session->get('cart', []);
        if ($all == true)
        {
            unset($cart[$id]);
        } else {
            if($order = $orderRepo->find($id))
            {
                if($cart[$order->getId()] > 1)
                {
                    $cart[$order->getId()] -= 1;
                } else {
                    unset($cart[$order->getId()]);
                }
            }
            
        }
        $session->set('cart',$cart);
        return $this->redirectToRoute('app_home');
    }
    #[Route('/cart', name: 'cart')]
    public function show(
        ManagerRegistry $doctrine, 
        SessionInterface $session, 
        ProductRepository $productRepo, 
        Mailer $email, 
        Request $request
    ): Response {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg() * $quantity;
        }
        $facture = new Facture();
        $form = $this->createFormBuilder($facture)
            ->add('reference', HiddenType::class)
            ->add('email', EmailType::class)
            ->add('date', HiddenType::class)
            ->add('save', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            
            // La reference
            $facture->generate();
            // Le panier 
            $facture->setCart($cart);
            // La date de création
            $facture->setDate(new \DateTime('tomorrow'));
            //on persist(); et flush();
            $entityManager->persist($facture);
            $entityManager->flush();
            // On récup les données
            $dataForm = $form->getData();
            // Puis on envoie le mail
            $email->send(
                'no-reply@gmail.com',
                $dataForm->getEmail(),
                'Commande passé avec succès',
                'index',
                compact('dataForm')
                    
            );

            // ... perform some action, such as saving the task to the database
            $this->addFlash('sucess','La commande a été passé avec succès, un mailvous a été envoyé à l\'adresse suivante :'.$dataForm->getEmail());
            
        }

        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            'form' => $form,
            'total' => $total,
            'data' => $data
        ]);
    }
}
