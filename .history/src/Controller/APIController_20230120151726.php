<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\OrderRepository;
use App\Entity\Order;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends AbstractController
{
    #[Route('/api_get_cart', name: 'app_api_get_cart')]
    public function getCart(SessionInterface $session,OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPricePerKg() * $quantity;
        }
        $response = new Response(json_encode($data));
        return $response;
    }
    #[Route('/api_add_cart', name: 'app_api_get_cart')]
    public function addCart(SessionInterface $session,OrderRepository $orderRepo): Response
    {
        
        $response = new Response(json_encode($data));
        return $response;
    }
    #[Route('/api_empty_cart', name: 'app_api_get_cart')]
    public function emptyCart(SessionInterface $session,OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPricePerKg() * $quantity;
        }
        $response = new Response(json_encode($data));
        return $response;
    }
}
