<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart', name: 'cart_add')]
    public function add(SessionInterface $session): Response
    {
        $cart =$session->get('cart', []);
        $cart[$_GET['product']] = $_GET['quantity'];
        
        $session->set('cart',$cart);
        dd($session->get('cart'));
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'cart' => $session->get('cart')
        ]);
    }
}
