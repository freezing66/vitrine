<?php

namespace App\Controller;

use App\Repository\OrderRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart_add', name: 'cart_add')]
    public function add(SessionInterface $session): Response
    {
        $cart =$session->get('cart', []);
        $cart[$_GET['product']] = $_GET['quantity'];
        
        $session->set('cart',$cart);
        return $this->redirectToRoute('app_home');
    }
    #[Route('/cart', name: 'cart')]
    public function show(SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product6->getPricePerKg();
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'cartController',
            
        ]);
    }
}
