<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Order;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $product = $doctrine->getRepository(Order::class)->findAll();
        dd($product);
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'products' => $product
        ]);
    }
}
