<?php

namespace App\Controller;

use App\Service\Email;
use App\Form\Mailer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    #[Route('/email', name: 'app_email')]
    public function index(Email $email,Facture $facture): Response
    {
        new \DateTime('tomorrow');

        $form = $this->createForm(Mailer::class, $task);

        $email->send(
            'symfonymatete@gmail.com',
            $user->getEmail(),
            'Password Confirmation',
            'confirmation_email',
            compact('user','token')
        );

        return $this->render('email/index.html.twig', [
            'controller_name' => 'EmailController',
        ]);
    }
}
