<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\OrderRepository;
use App\Entity\Order;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends AbstractController
{
    #[Route('/api_get_cart', name: 'app_api_get_cart')]
    public function getCart(SessionInterface $session,OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;
        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product->getName(),
                "quantity" => $quantity
            ];
           $total += $product->getPricePerKg() * $quantity;
        }
        $response = new Response(json_encode($data));
        return $response;
    }

    #[Route('/api_add_cart?id={id}', name: 'app_api_get_cart')]
    public function addCart($id, SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart=$session->get('cart', []);
        if (empty($cart[$order->getId()]))
        {
            $cart[$order->getId()] = 0;
        }
        $cart[$order->getId()] += 1;
        $session->set('cart',$cart);
        $response = new Response(true);
        return $response;
    }

    #[Route('/api_delete_cart?id={id}', name: 'app_api_get_cart')]
    public function deleteToCart($id, SessionInterface $session, OrderRepository $orderRepo): Response
    {
        $cart = $session->get('cart', []);
        if($order = $orderRepo->find($id))
        {
            if($cart[$order->getId()] > 1)
            {
                $cart[$order->getId()] -= 1;
            } else {
                unset($cart[$order->getId()]);
            }
        }
            
        }
        $session->set('cart',$cart);
        $response = new Response(true);
        return $response;
    }

    #[Route('/api_empty_cart', name: 'app_api_get_cart')]
    public function emptyCart(SessionInterface $session): Response
    {
        $response = new Response();
        if(!$session->set('cart', [])){
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }
}
