<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cart', name :'cart_')]
class CartController extends AbstractController
{
    #[Route('/', name: 'add')]
    public function add(): Response
    {
        if (isset($_GET['product']) && isset($_GET['quantity'])){
            return $this->render('cart/index.html.twig', [
                'controller_name' => 'CartController',
                'info' => [$_GET['product'],$_GET['quantity']]
            ]);
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'info' => ['salut','quantity']
        ]);
    }
    #[Route('/', name: 'show')]
    public function show(): Response
    {
        if (isset($_GET['product']) && isset($_GET['quantity'])){
            return $this->render('cart/index.html.twig', [
                'controller_name' => 'CartController',
                'info' => [$_GET['product'],$_GET['quantity']]
            ]);
        }
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
            'info' => ['salut','quantity']
        ]);
    }
}
