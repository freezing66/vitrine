<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends AbstractController
{
    #[Route('/api?format={format}', name: 'app_api_get_cart')]
    public function index($format): Response
    {
        $cart = $session->get('cart', []);
        $data = [];
        $total = 0;

        foreach($cart as $id => $quantity)
        {
            $product = $orderRepo->find($id);
            $data[] = [
                "product" => $product,
                "quantity" => $quantity
            ];
            $total += $product->getPricePerKg() * $quantity;
        }
        if ($format == 'json') {
            $data = array(
                'attending' => true
            );

            $response = new Response(json_encode($data));

            return $response;
        }

        return $this->render('api/index.html.twig', [
            'controller_name' => 'APIController',
        ]);
    }
}
