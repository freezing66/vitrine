<?php
namespace App\Form;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Mailer extends AbstractController
{
    public function new(FormBuilderInterface $builder, array $options, Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $builder
            ->add('reference', TextType::class)
            ->add('mail', EmailType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class)
        ;

        $form = $this->createFormBuilder($task)
            ->add('task', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Task'])
            ->getForm();

        // ...
    }
}