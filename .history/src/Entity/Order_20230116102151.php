<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?float $PricePerKg = null;

    #[ORM\Column(nullable: true)]
    private ?float $PricePerUnit = null;

    #[ORM\Column(length: 255)]
    private ?string $origin = null;

    #[ORM\Column(nullable: false)]
    private ?bool $unit = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPricePerKg(): ?float
    {
        return $this->PricePerKg;
    }

    public function setPricePerKg(?float $PricePerKg): self
    {
        $this->PricePerKg = $PricePerKg;

        return $this;
    }

    public function getPricePerUnit(): ?float
    {
        return $this->PricePerUnit;
    }

    public function setPricePerUnit(?float $PricePerUnit): self
    {
        $this->PricePerUnit = $PricePerUnit;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }
}
