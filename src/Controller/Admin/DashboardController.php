<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Facture;
use App\Entity\Product;
use App\Entity\Category;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator,
        private Security $security
    ){

    }
    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        if (($user = $this->security->getUser())) {
            $url = $this->adminUrlGenerator
                ->setController(ProductCrudController::class)
                ->generateUrl();
            return $this->redirect($url); 
        }
        return $this->redirectToRoute('app_home'); 
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Vitrine Traiteur Intermarche');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('accueil', 'fa fa-home');

        yield MenuItem::section('Category');

        yield MenuItem::subMenu('Category','fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Add Cat','fas fa-plus', Category::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show Cats','fas fa-eye', Category::class)
        ]);

        yield MenuItem::section('Product');

        yield MenuItem::subMenu('Product','fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Add Product','fas fa-plus', Product::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show Products','fas fa-eye', Product::class)
        ]);

        yield MenuItem::section('Facture');

        yield MenuItem::linkToCrud('Show Factures','fas fa-eye', Facture::class);

        yield MenuItem::section('User');

        yield MenuItem::linkToCrud('Show Users','fas fa-eye', User::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
